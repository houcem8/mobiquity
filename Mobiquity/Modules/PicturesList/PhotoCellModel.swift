//
//  PhotoCellModel.swift
//  Mobiquity
//
//  Created by Houcem on 25/10/2020.
//  Copyright © 2020 Houcem. All rights reserved.
//

import Foundation

enum PhotoCellModel: Hashable {
    case photoCell(photo: Photo)
    
    //MARK: Hashable
    func hash(into hasher: inout Hasher){
        hasher.combine(rawValue)
    }
    
    //MARK: Equatable
    static func == (lhs: PhotoCellModel, rhs: PhotoCellModel) -> Bool {
        return lhs.rawValue == rhs.rawValue
    }
    
    private var rawValue : String {
        switch self {
        case .photoCell:
            return "photoCell"
        }
    }
}
