//
//  SearchViewController.swift
//  Mobiquity
//
//  Created by Houcem on 24/10/2020.
//  Copyright © 2020 Houcem. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var viewModel: SearchViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel?.showLoader = { shouldShow in
            shouldShow ? self.activityIndicator.startAnimating() : self.activityIndicator.stopAnimating()
        }
        
        searchBar.setShowsCancelButton(true, animated: true)
        searchBar.searchTextField.becomeFirstResponder()
        searchBar.tintColor = .gray
        searchBar.enablesReturnKeyAutomatically = true
        searchBar.delegate = self
        
        tableView.keyboardDismissMode = .onDrag
        tableView.dataSource = self
        tableView.delegate = self
    }

}

extension SearchViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel?.getSearchHistory().count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let item = viewModel?.getSearchItem(at: indexPath.row)
        cell.textLabel?.text = item?.keyword
        return cell
    }
}

extension SearchViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = viewModel?.getSearchItem(at: indexPath.row)
        viewModel?.searchPictures(with: item?.keyword)
    }
    
}

extension SearchViewController: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        viewModel?.dismissSearchScreen()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        viewModel?.searchPictures(with: searchBar.text)
    }
}
