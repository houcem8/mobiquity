//
//  SearchItem.swift
//  Mobiquity
//
//  Created by Houcem on 24/10/2020.
//  Copyright © 2020 Houcem. All rights reserved.
//

import Foundation

struct SearchItem: Codable {
    var keyword: String?
    var date: Date?
}
