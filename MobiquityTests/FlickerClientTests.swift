//
//  FlickerClientTests.swift
//  MobiquityTests
//
//  Created by Houcem on 26/10/2020.
//  Copyright © 2020 Houcem. All rights reserved.
//

import XCTest
@testable import Mobiquity

extension FlickerClient {
    static let mock = FlickerClient(baseUrl: "https://flickr.com/services/rest/",
                                    apiKey: "a09ceda8a9522fd917cc016048e213e2")
}

class FlickerClientTests: XCTestCase {
    
    override func setUpWithError() throws {
        Current = .mock
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testSearchImages_success() {
        let exp = expectation(description: "elements returned not nil")
        Current.flickerClient.searchImages("Flowers", 1) { result in
            guard case .success(let photos) = result else { XCTAssert(false) ; return }
            exp.fulfill()
            XCTAssertNotNil(photos)
        }
        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error { XCTFail("Failed on timeout with error \(error)") }
        }
    }
    
    func testSearchImages_no_keyword() {
        let exp = expectation(description: "return error == invalidResponse")
        Current.flickerClient.searchImages("", 1) { result in
            guard case .failure(let error) = result else { XCTAssert(false) ; return }
            exp.fulfill()
            XCTAssert(error == .invalidResponse)
        }
        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error { XCTFail("Failed on timeout with error \(error)") }
        }
    }
    
    func testSearchImages_invalid_keyword() {
        let exp = expectation(description: "return error == invalidResponse")
        Current.flickerClient.searchImages("** ** ** *", 1) { result in
            guard case .failure(let error) = result else { XCTAssert(false) ; return }
            exp.fulfill()
            XCTAssert(error == .noResultFound)
        }
        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error { XCTFail("Failed on timeout with error \(error)") }
        }
    }
    
    func testGenerateImageUrl() {
        let url = FlickerClient.getImageUrl(frame: 1, server: "1", id: "2", secret: "3")
        XCTAssertEqual(url, "https://farm1.static.flickr.com/1/2_3.jpg")
    }
    
}
