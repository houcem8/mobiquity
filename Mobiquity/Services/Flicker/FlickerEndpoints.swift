//
//  FlickerEndpoints.swift
//  Mobiquity
//
//  Created by Houcem on 25/10/2020.
//  Copyright © 2020 Houcem. All rights reserved.
//

import Foundation

struct FlickerEndpoint {
    var type: FlickerEndpointType
}

enum FlickerEndpointType {
    case searchImages(keyword: String?, page: Int)
    
    var value: URL? {
        switch self {
        case .searchImages(let keyword, let page):
            guard   let url = Current.flickerClient.baseUrl,
                    let key = Current.flickerClient.apiKey,
                    let keyword = keyword else {
                return nil
            }
            let pageCapacity = Current.flickerClient.pageCapacity

            let queryItems = [
                URLQueryItem(name: "method", value: "flickr.photos.search"),
                URLQueryItem(name: "api_key", value: key),
                URLQueryItem(name: "text", value: keyword),
                URLQueryItem(name: "page", value: String(page)),
                URLQueryItem(name: "per_page", value: String(pageCapacity)),
                URLQueryItem(name: "format", value: "json"),
                URLQueryItem(name: "nojsoncallback", value: "1")
            ]
            var components = URLComponents(string: url)
            components?.queryItems = queryItems
            return components?.url
        }
    }
    
}
