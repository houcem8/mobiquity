//
//  SearchViewModelTests.swift
//  MobiquityTests
//
//  Created by Houcem on 26/10/2020.
//  Copyright © 2020 Houcem. All rights reserved.
//

import XCTest

class SearchViewModelTests: XCTestCase {

    override func setUpWithError() throws {
        Current = .mock
        Current.storage.saveSearch(items: [])
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testGetSearchHistory() {
        Current.storage.addSearch(item: SearchItem(keyword: "Dog", date: Date()))
        Current.storage.addSearch(item: SearchItem(keyword: "Cat", date: Date()))
        Current.storage.addSearch(item: SearchItem(keyword: "Horse", date: Date()))

        let viewModel = SearchViewModel(storage: .prod, flickerClient: .mock)
        
        let itemsSaved = viewModel.getSearchHistory()
        
        XCTAssertEqual(itemsSaved.count, Current.storage.loadSearchHistory().count)
        XCTAssertEqual(itemsSaved.first?.keyword, Current.storage.loadSearchHistory().first?.keyword)
    }
    
    func testGetSearchItem() {
        Current.storage.addSearch(item: SearchItem(keyword: "Dog", date: Date()))
        Current.storage.addSearch(item: SearchItem(keyword: "Cat", date: Date()))
        
        let viewModel = SearchViewModel(storage: .prod, flickerClient: .mock)
        _ = viewModel.getSearchHistory()
        
        XCTAssertNotNil(viewModel.getSearchItem(at: 0))
        XCTAssertNotNil(viewModel.getSearchItem(at: 1))
        XCTAssertNil(viewModel.getSearchItem(at: 3))
        XCTAssertNil(viewModel.getSearchItem(at: -1))
    }

}
