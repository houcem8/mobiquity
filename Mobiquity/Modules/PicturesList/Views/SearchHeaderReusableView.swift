//
//  SearchHeaderReusableView.swift
//  Mobiquity
//
//  Created by Houcem on 23/10/2020.
//  Copyright © 2020 Houcem. All rights reserved.
//

import UIKit

class SearchHeaderReusableView: UICollectionReusableView {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var searchLabel: UILabel!
    
    lazy var didTapSearchButton: ()->() = {}
    
    static var reuseIdentifier: String {
        return String(describing: SearchHeaderReusableView.self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.cornerRadius = 10
    }
    
    func setupView(title: String?) {
        searchLabel.text = title ?? "Discover the world from search box"
    }
    
    @IBAction func searchAction(_ sender: Any) {
        
        didTapSearchButton()
    }
    
}
