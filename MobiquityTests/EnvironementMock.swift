//
//  EnvironementMock.swift
//  MobiquityTests
//
//  Created by Houcem on 26/10/2020.
//  Copyright © 2020 Houcem. All rights reserved.
//

import Foundation
@testable import Mobiquity

extension Environement {
    static let mock = Environement(storage: .prod,
                                   flickerClient: .mock)
}
