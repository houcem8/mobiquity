//
//  SearchImagesViewController.swift
//  Mobiquity
//
//  Created by Houcem on 23/10/2020.
//  Copyright © 2020 Houcem. All rights reserved.
//

import UIKit

class PicturesListViewController: UICollectionViewController {
    
    // MARK: - Properties
    var viewModel: PicturesListViewModel?
    
    var isLoading = false //Indication wether or not the screen is loading data from network
    private lazy var dataSource: DataSource = createDataSource()
    
    // MARK: - Value Types
    typealias DataSource = UICollectionViewDiffableDataSource<Section, Photo>
    typealias Snapshot = NSDiffableDataSourceSnapshot<Section, Photo>
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupViewModel()
        applySnapshot(animatingDifferences: false)
    }
    
    func setupView() {
        title = "Search"
        
        collectionView.prefetchDataSource = self
        collectionView.register(UINib(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageCollectionViewCell")
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.sectionHeadersPinToVisibleBounds = true
        }
    }
    
    func setupViewModel() {
        viewModel?.resetView = {
            self.collectionView.scrollsToTop = true
            self.collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
            
        }
        viewModel?.refreshView = {
            self.applySnapshot(animatingDifferences: true)
            self.isLoading = false
        }
    }

    
    func createDataSource() -> DataSource {
        let dataSource = DataSource(collectionView: collectionView,cellProvider: {
            (collectionView, indexPath, video) -> UICollectionViewCell? in
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell",for: indexPath) as? ImageCollectionViewCell
            
            let section = self.dataSource.snapshot().sectionIdentifiers[indexPath.section]
            
            cell?.setupCell(photo: section.pictures[indexPath.row])
            return cell
        })
        
        dataSource.supplementaryViewProvider = { [unowned self] collectionView, kind, indexPath in
            guard kind == UICollectionView.elementKindSectionHeader else {
                return nil
            }
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,withReuseIdentifier: "SearchHeaderReusableView" ,for: indexPath) as? SearchHeaderReusableView
            headerView?.didTapSearchButton = self.didTapSearch
            headerView?.setupView(title: self.viewModel?.keyword)
            return headerView
        }
        
        return dataSource
    }
    
    func applySnapshot(animatingDifferences: Bool = true) {
        var snapshot = Snapshot()
        let sections = viewModel?.getSections() ?? []
        snapshot.appendSections(sections)
        sections.forEach { section in
            snapshot.appendItems(section.pictures, toSection: section)
        }
        dataSource.apply(snapshot, animatingDifferences: animatingDifferences)
    }
    
    func didTapSearch() {
        viewModel?.showSearchScreen()
    }
}

extension PicturesListViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 70)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = UIScreen.main.bounds.width/2 - 1
        let cellHeight = cellWidth * 7/5
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
    }
}

extension PicturesListViewController: UICollectionViewDataSourcePrefetching {
    
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        guard !isLoading else {
            return
        }
        let lastPageIndex = dataSource.snapshot().numberOfItems - collectionView.visibleCells.count
        if indexPaths[0].row >= lastPageIndex {
            isLoading = true
            viewModel?.getNextPage()
        }
    }
}
