//
//  SearchImagesCoordinator.swift
//  Mobiquity
//
//  Created by Houcem on 23/10/2020.
//  Copyright © 2020 Houcem. All rights reserved.
//

import Foundation
import UIKit

class PicturesListCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    private var navigationController : UINavigationController
    
    var onSearchFinishEvent: (Photos?, String?)->() = { _, _ in }
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }

            guard let viewController = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: "PicturesListViewController") as? PicturesListViewController else {
                    return
            }
            let viewModel = PicturesListViewModel(flickerClient: Current.flickerClient)
            viewController.viewModel = viewModel
            viewController.viewModel?.coordinator = self
            
            self.onSearchFinishEvent = viewModel.displaySearchedPhotos
            self.navigationController.setViewControllers([viewController], animated: false)
        }
    }
    
    func showSearchScreen() {
        let searchCoordinator = SearchCoordinator(navigationController: navigationController)
        searchCoordinator.parentCoordinator = self
        childCoordinators.append(searchCoordinator)
        searchCoordinator.start()
    }
    
}
