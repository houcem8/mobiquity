//
//  Flicker.swift
//  Mobiquity
//
//  Created by Houcem on 24/10/2020.
//  Copyright © 2020 Houcem. All rights reserved.
//

import Foundation

class FlickerClient {
    static let dev = FlickerClient(baseUrl: "https://flickr.com/services/rest/",
                                   apiKey: "a09ceda8a9522fd917cc016048e213e2")
    
    var baseUrl: String?
    var apiKey: String?
    var pageCapacity: Int
    var searchImages: (String?,Int, @escaping (Result<Photos, NetworkError>) -> Void) -> Void

    private let networkService: NetworkDataFetcher = NetworkDataFetcher()
    
    init(baseUrl: String ,apiKey: String, pageCapacity: Int = 100) {
        self.baseUrl = baseUrl
        self.apiKey = apiKey
        self.pageCapacity = pageCapacity
        
        searchImages = searchImages(keyword:page:completion:)
    }
    
    static func getImageUrl(frame: Int, server: String, id: String, secret: String) -> String?{
        
        return "https://farm\(frame).static.flickr.com/\(server)/\(id)_\(secret).jpg"
    }
}

func searchImages(keyword: String?, page: Int = 1, completion: @escaping (Result<Photos, NetworkError>) -> Void) {
    let searchImagesEndPoint = FlickerEndpoint(type: .searchImages(keyword: keyword, page: page))

    guard let _ = keyword,
          let url = searchImagesEndPoint.type.value  else {
          completion(.failure(.invalidParameters))
          return
    }
    
    let networkService = NetworkDataFetcher(session: URLSession.shared)
    networkService.performRequest(url: url) { (result) in
        switch result {
            
        case .success(let data):
            do{
                let picturesResult = try JSONDecoder().decode(PicturesResult.self, from: data)
                if picturesResult.photos.photo.count == 0 {
                    //ERROR: if no result returned
                    completion(.failure(.noResultFound))
                }else {
                    //SUCCESS
                    completion(.success(picturesResult.photos))
                }
            } catch {
                //ERROR: if json decoder fails
                completion(.failure(.invalidResponse))
            }
        case .failure:
            //ERROR: if request fails
            completion(.failure(.invalidResponse))
            break
        }
    }
}
