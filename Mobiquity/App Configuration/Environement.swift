//
//  Environement.swift
//  Mobiquity
//
//  Created by Houcem on 24/10/2020.
//  Copyright © 2020 Houcem. All rights reserved.
//

import Foundation

//* wrap all project dependencies in the Environement struct
struct Environement {
    
    private(set) var storage: StorageService = .prod
    private(set) var flickerClient: FlickerClient = .dev

}

var Current = Environement()
