//
//  PictureResult.swift
//  Mobiquity
//
//  Created by Houcem on 25/10/2020.
//  Copyright © 2020 Houcem. All rights reserved.
//

import Foundation

// MARK: - PicturesResult
class PicturesResult: Codable {
    let photos: Photos
    let stat: String
}

// MARK: - Photos
class Photos: Codable {
    let page: Int?
    let pages: Int?
    let perpage: Int?
    let total: String?
    var photo: [Photo]
    
    init(page: Int, pages: Int, perpage: Int, total: String, photos: [Photo]) {
        self.page = page
        self.pages = pages
        self.perpage = perpage
        self.total = total
        self.photo = photos
    }
}
