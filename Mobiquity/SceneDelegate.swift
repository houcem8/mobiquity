//
//  SceneDelegate.swift
//  Mobiquity
//
//  Created by Houcem on 22/10/2020.
//  Copyright © 2020 Houcem. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    var appCoordinator : AppCoordinator?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
      
        guard let windowScene = (scene as? UIWindowScene) else { return }
                
        let appWindow = UIWindow(frame: windowScene.coordinateSpace.bounds)
        appWindow.windowScene = windowScene
        window = appWindow
        
        appCoordinator = AppCoordinator(window: appWindow)
        appCoordinator?.start()
    }

}

