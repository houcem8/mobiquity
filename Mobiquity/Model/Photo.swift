//
//  Photo.swift
//  Mobiquity
//
//  Created by Houcem on 23/10/2020.
//  Copyright © 2020 Houcem. All rights reserved.
//

import Foundation

// MARK: - Photo
class Photo: Codable, Hashable {
    let id, owner, secret, server: String
    let farm: Int
    let title: String
    
    init(id: String, owner: String, secret: String, server: String, fram: Int, title: String) {
        self.id = id
        self.owner = owner
        self.secret = secret
        self.server = server
        self.farm = fram
        self.title = title
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    static func == (lhs: Photo, rhs: Photo) -> Bool {
        lhs.id == rhs.id
    }
}

extension Photos {
    static var mock : Photos = Photos(page: 1,
                                      pages: 200,
                                      perpage: 100,
                                      total: "20000",
                                      photos: [Photo(id: "1", owner: "12Z2", secret: "1233", server: "1222", fram: 1, title: "photo1"),
                                               Photo(id: "14", owner: "12AZ2", secret: "12233", server: "132", fram: 1, title: "photo2"),
                                               Photo(id: "19", owner: "122d2", secret: "122ff3", server: "1322", fram: 1, title: "photo3"),
                                                ])
}
