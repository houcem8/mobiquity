//
//  ImageCollectionViewCell.swift
//  Mobiquity
//
//  Created by Houcem on 23/10/2020.
//  Copyright © 2020 Houcem. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setupCell(photo: Photo) {
        let url = FlickerClient.getImageUrl(frame: photo.farm, server: photo.server, id: photo.id, secret: photo.secret)
        imageView.setImage(withUrl: url)
    }
}
