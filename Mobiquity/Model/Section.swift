//
//  Section.swift
//  Mobiquity
//
//  Created by Houcem on 23/10/2020.
//  Copyright © 2020 Houcem. All rights reserved.
//

import Foundation
import UIKit

class Section: Hashable {
    var id = UUID()
    var title: String
    var pictures: [Photo]
    
    init(title: String, pictures: [Photo]) {
        self.title = title
        self.pictures = pictures
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    static func == (lhs: Section, rhs: Section) -> Bool {
        lhs.id == rhs.id
    }
}
