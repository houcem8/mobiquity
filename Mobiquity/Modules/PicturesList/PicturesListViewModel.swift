//
//  SearchImagesViewModel.swift
//  Mobiquity
//
//  Created by Houcem on 23/10/2020.
//  Copyright © 2020 Houcem. All rights reserved.
//

import Foundation
import UIKit

class PicturesListViewModel {
    
    var coordinator: PicturesListCoordinator?
    
    var photos: Photos?
    var keyword: String?
    
    var resetView = {}
    var refreshView = {}
    
    private var page = 1
    private var flickerClient: FlickerClient
    private var sections : [Section] = [Section(title: "Search", pictures: [])]

    
    init(flickerClient: FlickerClient) {
        self.flickerClient = flickerClient
    }
    
    //Display fetched photos returned from search screen
    func displaySearchedPhotos(_ photos: Photos?, keyword: String?) {
        guard let photos = photos else {
            return
        }
        self.photos = photos
        self.keyword = keyword
        self.page = 1

        sections = [Section(title: "Search", pictures: photos.photo)]
        resetView()
        refreshView()
    }
    
    func getSections() -> [Section] {
        sections
    }
    
    func showSearchScreen() {
        coordinator?.showSearchScreen()
    }
    
    //Load next photos page
    func getNextPage() {
        guard page < (photos?.pages ?? 1) else {
            return
        }
        
        page += 1
        flickerClient.searchImages(keyword, page) { [unowned self] result in
            switch result {
            case .success(let photos):
                self.photos?.photo += photos.photo
                self.sections = [Section(title: "Search", pictures: self.photos?.photo ?? [])]
                self.refreshView()
            case .failure(let error):
                print(error)
                break
            }
        }
    }
}
