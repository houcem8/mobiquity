//
//  Storage.swift
//  Mobiquity
//
//  Created by Houcem on 24/10/2020.
//  Copyright © 2020 Houcem. All rights reserved.
//

import Foundation

extension StorageService {
    static let prod = StorageService(withPath: "SearchItems")
}

final class StorageService {
    let path: String
    
    init(withPath path: String? = nil) {
        let pathName = path ?? "documents"
        self.path = "/\(pathName).json"
    }
    
    //* Save search items in storage
    func saveSearch(items: [SearchItem]) {
        guard let documentsDir = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
            else { return }
        save(items, to: documentsDir.appending(path))
    }

    //* get saved search items froms storage
    func loadSearchHistory() -> [SearchItem] {
        guard let documentsDir = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
        else { return [SearchItem]() }

        if let storedSearchItems: [SearchItem] = load(from: documentsDir.appending(path)) {
             return storedSearchItems
        } else {
            return [SearchItem]()
        }
    }
    
    //* Add Search item at first position and save it
    //* if item already exists, move it to first position
    func addSearch(item: SearchItem) {
        let items = loadSearchHistory()
        var filtredItems = items.filter { $0.keyword != item.keyword }
        filtredItems.insert(item, at: 0)
        saveSearch(items: filtredItems)
    }
}

private extension StorageService {
    func load<T: Decodable>(from path: String, as type: T.Type = T.self) -> T? {
        let data: Data

        do {
            data = try Data(contentsOf: URL(fileURLWithPath: path))
        } catch {
            debugPrint("Couldn't load from main bundle")
            return nil
        }

        do {
            let decoder = JSONDecoder()
            return try decoder.decode(T.self, from: data)
        } catch {
            debugPrint("Couldn't parse Data")
            return nil
        }
    }

    func save<T: Encodable>(_ data: T, to path: String) {
        let url = URL(fileURLWithPath: path)
        do {
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            let JsonData = try encoder.encode(data)
            try JsonData.write(to: url)
        } catch {
            debugPrint("Couldn't save to \(path)")
        }
    }
}
