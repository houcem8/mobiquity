//
//  StorageServiceTests.swift
//  MobiquityTests
//
//  Created by Houcem on 24/10/2020.
//  Copyright © 2020 Houcem. All rights reserved.
//

import XCTest
@testable import Mobiquity

class StorageServiceTests: XCTestCase {
    
    var storage: StorageService = StorageService()
    
    override func setUpWithError() throws {
        storage.saveSearch(items: [])
        
    }
    
    override func tearDownWithError() throws {
        
    }
    
    func testLoadSearchHistory_empty() {
        let storedItems = storage.loadSearchHistory()
        XCTAssertTrue(storedItems.isEmpty)
    }
    
    func testLoadSearchHistory_not_empty() {
        let items = [SearchItem(keyword: "Flowers", date: Date()),
                     SearchItem(keyword: "Lion", date: Date()),
                     SearchItem(keyword: "dog", date: Date())
        ]
        storage.saveSearch(items: items)
        
        let storedItems = storage.loadSearchHistory()
        XCTAssertFalse(storedItems.isEmpty)
        XCTAssertEqual(storedItems.count, 3)
    }
    
    func testSaveSearch_empty_items() {
        storage.saveSearch(items: [])
        
        let storedItems = storage.loadSearchHistory()
        XCTAssertTrue(storedItems.isEmpty)
    }
    
    func testSaveSearch_items() {
        let items = [SearchItem(keyword: "Flowers", date: Date()),
                     SearchItem(keyword: "Lion", date: Date()),
                     SearchItem(keyword: "dog", date: Date())
        ]
        storage.saveSearch(items: items)
        
        let storedItems = storage.loadSearchHistory()
        XCTAssertFalse(storedItems.isEmpty)
        XCTAssertEqual(storedItems.count, 3)
    }
    
    func testAddSearch_new_item(){
        let items = [SearchItem(keyword: "Flowers", date: Date()),
                     SearchItem(keyword: "Lion", date: Date()),
                     SearchItem(keyword: "dog", date: Date())
        ]
        storage.saveSearch(items: items)
        
        storage.addSearch(item: SearchItem(keyword: "Cat", date: Date()))
        
        let storedItems = storage.loadSearchHistory()
        XCTAssertEqual(storedItems.count, 4)
        XCTAssertEqual(storedItems.first?.keyword, "Cat")
    }
    
    func testAddSearch_existing_item(){
        let items = [SearchItem(keyword: "Flowers", date: Date()),
                     SearchItem(keyword: "Lion", date: Date()),
                     SearchItem(keyword: "dog", date: Date())
        ]
        storage.saveSearch(items: items)
        
        storage.addSearch(item: SearchItem(keyword: "dog", date: Date()))
        
        let storedItems = storage.loadSearchHistory()
        XCTAssertEqual(storedItems.count, 3)
        XCTAssertEqual(storedItems.first?.keyword, "dog")
    }
}
