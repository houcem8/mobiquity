//
//  AppCoordinator.swift
//  Mobiquity
//
//  Created by Houcem on 23/10/2020.
//  Copyright © 2020 Houcem. All rights reserved.
//

import Foundation
import UIKit

protocol Coordinator : class {
    var childCoordinators : [Coordinator] { get set }
    
    func start()
    func childDidFinish(childCoordinator: Coordinator)
}

class AppCoordinator : Coordinator {
    var childCoordinators : [Coordinator] = []
    private var navigationController : UINavigationController!
    private let window : UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        navigationController = UINavigationController()
        
        navigationController.navigationBar.isHidden = false
        navigationController.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController.navigationBar.shadowImage = UIImage()
        navigationController.navigationBar.isTranslucent = false
        navigationController.navigationBar.prefersLargeTitles = true
        navigationController.navigationBar.backgroundColor = UIColor(named: "lightGray")
        navigationController.view.backgroundColor = UIColor(named: "lightGray")
        navigationController.navigationBar.barTintColor = UIColor(named: "lightGray")
        
        let searchImagesCoordinator = PicturesListCoordinator(navigationController: navigationController)
        childCoordinators.append(searchImagesCoordinator)
        searchImagesCoordinator.start()
        
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
}

extension Coordinator {
    func childDidFinish(childCoordinator: Coordinator) {
        if let index = childCoordinators.firstIndex(where: {
            (coordinator: Coordinator) -> Bool in
            childCoordinator === coordinator
        }) {
            childCoordinators.remove(at: index)
        }
    }
}
