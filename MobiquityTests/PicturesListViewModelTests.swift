//
//  PicturesListViewModelTests.swift
//  MobiquityTests
//
//  Created by Houcem on 26/10/2020.
//  Copyright © 2020 Houcem. All rights reserved.
//

import XCTest
@testable import Mobiquity

class PicturesListViewModelTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testDisplaySearchedPhotos_equal_to_sections_cell() throws {
        let viewModel = PicturesListViewModel(flickerClient: .dev)
        let photos: Photos = Photos.mock
        viewModel.displaySearchedPhotos(photos, keyword: "flower")
        
        let sectionCells = viewModel.getSections().first?.pictures.count
        XCTAssertEqual(Photos.mock.photo.count, sectionCells)
    }
}

