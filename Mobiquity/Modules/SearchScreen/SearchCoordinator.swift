//
//  SearchCoordinator.swift
//  Mobiquity
//
//  Created by Houcem on 24/10/2020.
//  Copyright © 2020 Houcem. All rights reserved.
//

import Foundation
import UIKit

class SearchCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    private var navigationController : UINavigationController
    
    var parentCoordinator: Coordinator?

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        guard let viewController = UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController else {
                return
        }
        viewController.viewModel = SearchViewModel(storage: Current.storage,
                                                   flickerClient: Current.flickerClient)
        viewController.viewModel?.coordinator = self
        
        viewController.modalPresentationStyle = .currentContext
        
        navigationController.modalPresentationStyle = .fullScreen
        navigationController.present(viewController, animated: false, completion: nil)
    }
    
    func didEndSearch(withPhotos photos: Photos? = nil, keyword: String? = nil) {
        guard let parentCoordinator = parentCoordinator as? PicturesListCoordinator else {
            return
        }
        parentCoordinator.onSearchFinishEvent(photos, keyword)
        parentCoordinator.childDidFinish(childCoordinator: self)
        navigationController.dismiss(animated: false, completion: nil)
    }
}
