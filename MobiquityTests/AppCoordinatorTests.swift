//
//  AppCoordinatorTests.swift
//  MobiquityTests
//
//  Created by Houcem on 26/10/2020.
//  Copyright © 2020 Houcem. All rights reserved.
//

import XCTest
@testable import Mobiquity

class AppCoordinatorTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }

    func testWindowIsKey() {
        let window = WindowStub()
        let coordinator = AppCoordinator(window: window)
        coordinator.start()
        XCTAssertTrue(window.makeKeyAndVisibleCalled)
        XCTAssertNotNil(window.rootViewController)
    }

}

private class WindowStub: UIWindow {
    var makeKeyAndVisibleCalled = false
    override func makeKeyAndVisible() {
        makeKeyAndVisibleCalled = true
    }
}
