//
//  SearchImagesViewController.swift
//  Mobiquity
//
//  Created by Houcem on 23/10/2020.
//  Copyright © 2020 Houcem. All rights reserved.
//

import UIKit

class PicturesListViewController: UICollectionViewController {
        
    // MARK: - Properties
    var viewModel: SearchImagesViewModel?

    private var sections = Section.allSections
    private lazy var dataSource: DataSource = createDataSource()
    
    // MARK: - Value Types
    typealias DataSource = UICollectionViewDiffableDataSource<Section, Picture>
    typealias Snapshot = NSDiffableDataSourceSnapshot<Section, Picture>
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        applySnapshot(animatingDifferences: false)
    }
    
    func setupView() {
        title = "Search"
        
        collectionView.register(UINib(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageCollectionViewCell")
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.sectionHeadersPinToVisibleBounds = true
        }
    }
    
    func createDataSource() -> DataSource {
        let dataSource = DataSource(collectionView: collectionView,cellProvider: {
            (collectionView, indexPath, video) -> UICollectionViewCell? in
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell",for: indexPath) as? ImageCollectionViewCell
            return cell
        })
        
        dataSource.supplementaryViewProvider = { [unowned self] collectionView, kind, indexPath in
            guard kind == UICollectionView.elementKindSectionHeader else {
                return nil
            }
            let section = self.dataSource.snapshot().sectionIdentifiers[indexPath.section]
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,withReuseIdentifier: "SearchHeaderReusableView" ,for: indexPath) as? SearchHeaderReusableView
            headerView?.didTapSearchButton = self.didTapSearch
            return headerView
        }
        
        return dataSource
    }
    
    func applySnapshot(animatingDifferences: Bool = true) {
        var snapshot = Snapshot()
        snapshot.appendSections(sections)
        sections.forEach { section in
            snapshot.appendItems(section.pictures, toSection: section)
        }
        dataSource.apply(snapshot, animatingDifferences: animatingDifferences)
    }
    
    func didTapSearch() {
        print("88888!!!!!88888")
    }
}

extension PicturesListViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 70)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = UIScreen.main.bounds.width/2 - 1
        let cellHeight = cellWidth * 7/5
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
    }
}
