//
//  SearchViewModel.swift
//  Mobiquity
//
//  Created by Houcem on 24/10/2020.
//  Copyright © 2020 Houcem. All rights reserved.
//

import Foundation

class SearchViewModel {
    
    var coordinator: SearchCoordinator?
    var showLoader: (Bool)-> Void = { _ in }

    private var storage: StorageService
    private var flickerClient: FlickerClient
    private var searchItems = [SearchItem]()
    
    init(storage: StorageService, flickerClient: FlickerClient) {
        self.storage = storage
        self.flickerClient = flickerClient
    }
    
    func getSearchHistory() -> [SearchItem] {
        searchItems = storage.loadSearchHistory()
        return searchItems
    }
    
    func getSearchItem(at index: Int) -> SearchItem? {
        guard index >= 0 && index < searchItems.count else {
            return nil
        }
        return searchItems[index]
    }
    
    func searchPictures(with keyword: String?) {
        guard let keyword = keyword, !keyword.isEmpty else {
            return
        }
        showLoader(true)
        flickerClient.searchImages(keyword, 1) { [unowned self] result in
            self.showLoader(false)
            switch result {
            case .success(let photos):
                self.storage.addSearch(item: SearchItem(keyword: keyword, date: Date()))
                self.coordinator?.didEndSearch(withPhotos: photos, keyword: keyword)
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    
    func dismissSearchScreen() {
        coordinator?.didEndSearch()
    }
    
}
