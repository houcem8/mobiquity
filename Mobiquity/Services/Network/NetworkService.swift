//
//  NetworkService.swift
//  Mobiquity
//
//  Created by Houcem on 24/10/2020.
//  Copyright © 2020 Houcem. All rights reserved.
//

import Foundation

// MARK: - Result type
typealias NetworkResult = Result<Data, NetworkError>

// MARK: - Protocol
protocol NetworkService {
    func performRequest(url: URL, completion: @escaping (NetworkResult) -> Void)
}

// MARK: - Implementation
final class NetworkDataFetcher: NetworkService {
    
    private let session: NetworkSession
    
    init(session: NetworkSession = URLSession.shared) {
        self.session = session
    }
    
    func performRequest(url: URL, completion: @escaping (NetworkResult) -> Void) {
        
        session.loadData(from: url) { (data, response, error) in
            let result: NetworkResult
            defer {
                DispatchQueue.main.async {
                    completion(result)
                }
            }
            guard error == nil,
                let response = response as? HTTPURLResponse, (200 ... 299).contains(response.statusCode),
                let data = data else {
                    result = .failure(.unknown)
                    return
            }
            result = .success(data)
        }
    }
}

protocol NetworkSession {
    func loadData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> Void)
}

extension URLSession: NetworkSession {
    
    func loadData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        let task = dataTask(with: url) { (data, response, error) in
            completion(data, response, error)
        }
        task.resume()
    }
}
